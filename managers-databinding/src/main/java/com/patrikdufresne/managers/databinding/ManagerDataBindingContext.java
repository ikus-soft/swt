/**
 * Copyright(C) 2013 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.patrikdufresne.managers.databinding;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateListStrategy;
import org.eclipse.core.databinding.UpdateSetStrategy;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.value.IObservableValue;

import com.patrikdufresne.managers.Managers;

/**
 * Specialized database binding context to support automated persistence.
 * 
 * @author Patrik Dufresne
 *
 */
public class ManagerDataBindingContext extends DataBindingContext {

    private Managers managers;

    public ManagerDataBindingContext(Managers managers) {
        this.managers = managers;
    }

    @Override
    protected <T, M> UpdateValueStrategy<T, M> createTargetToModelUpdateValueStrategy(IObservableValue<T> fromValue, IObservableValue<M> toValue) {
        return new ManagerUpdateValueStrategy<T, M>(managers);
    }

    @Override
    protected <T, M> UpdateListStrategy<T, M> createTargetToModelUpdateListStrategy(
            IObservableList<T> targetObservableList,
            IObservableList<M> modelObservableList) {
        return new ManagerUpdateListStrategy<T, M>(managers);
    }

    @Override
    protected <T, M> UpdateSetStrategy<T, M> createTargetToModelUpdateSetStrategy(IObservableSet<T> targetObservableSet, IObservableSet<M> modelObservableSet) {
        return new ManagerUpdateSetStrategy<T, M>(managers);
    }

}
