/**
 * Copyright(C) 2013 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.patrikdufresne.managers;

import org.eclipse.swt.widgets.Display;
import org.hibernate.exception.ConstraintViolationException;

/**
 * This exception is throw when any type of error occurred within the execution of a Manager.
 * 
 * @author Patrik Dufresne
 * 
 */
public class ManagerException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Create a new exception without cause.
     */
    public ManagerException() {

    }

    /**
     * Constructs a new exception with the specified detail message. The cause is not initialized, and may subsequently
     * be initialized by a call to {@link #initCause}.
     * 
     * @param message
     *            the detail message. The detail message is saved for later retrieval by the {@link #getMessage()}
     *            method.
     */
    public ManagerException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     * <p>
     * Note that the detail message associated with <code>cause</code> is <i>not</i> automatically incorporated in this
     * exception's detail message.
     * 
     * @param message
     *            the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
     * @param cause
     *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt>
     *            value is permitted, and indicates that the cause is nonexistent or unknown.)
     */
    public ManagerException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new exception with the specified cause and a detail message of
     * <tt>(cause==null ? null : cause.toString())</tt> (which typically contains the class and detail message of
     * <tt>cause</tt>). This constructor is useful for exceptions that are little more than wrappers for other
     * throwables (for example, {@link java.security.PrivilegedActionException}).
     * 
     * @param cause
     *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt>
     *            value is permitted, and indicates that the cause is nonexistent or unknown.)
     * @param cause
     */
    public ManagerException(Throwable cause) {
        super(cause);
    }

    /**
     * Check if the exception is a constraint violation exception.
     * <p>
     * It's recommended to use this function instead of checking the type of the cause.
     * 
     * @return True if this exception is a contraint violation exception.
     */
    public boolean isConstraintViolationException() {
        return getCause() instanceof ConstraintViolationException;
    }

    /**
     * Return true if the current exception is raised because the database accessible only in readonly.
     */
    public boolean isReadonlyExeption() {
        // Try to determine if the exception is related to "ReadOnly" Access.
        // This occur when trying to open a readonly file with "servermode".
        Throwable cause = getCause();
        while (cause != null) {
            if (cause.getMessage().contains("Feature not supported")) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    /**
     * Return true if the current exception is raised because the database is locked by another process.
     * 
     * @param exception
     * @return
     */
    public boolean isLockedException() {
        Throwable cause = getCause();
        while (cause != null) {
            if (cause.getMessage().contains("Locked by another process")
                    || cause.getMessage().contains("verrouillé")
                    || cause.getMessage().contains("Lock file exists")
                    || cause.getMessage().contains("The file is locked")) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    /**
     * Return true if the current exception is raised because the database is not found.
     * 
     * @return
     */
    public boolean isNotFoundException() {
        // To handle various exception message, we need to dig into the exception.
        Throwable cause = getCause();
        while (cause != null) {
            if (cause.getMessage().contains("not found")) {
                // Display a generic message for other error.
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

}
