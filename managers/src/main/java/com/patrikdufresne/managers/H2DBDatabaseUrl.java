/**
 * Copyright(C) 2013 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.patrikdufresne.managers;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class represent a database Url
 * 
 * Ref.: http://www.h2database.com/html/features.html
 * 
 * @author Patrik Dufresne
 * 
 */
public class H2DBDatabaseUrl {

    /**
     * File's protocol.
     */
    private static final String SCHEME_FILE = "file:";

    private static final String SCHEME_JDBC_H2_LOCAL1 = "jdbc:h2:";

    private static final String SCHEME_JDBC_H2_LOCAL2 = "jdbc:h2:file:";

    private static final String SCHEME_JDBC_H2_MEM = "jdbc:h2:mem:";

    private static final String SCHEME_JDBC_H2_TCP = "jdbc:h2:tcp:";

    private static final List<String> LOCAL = Arrays.asList(SCHEME_JDBC_H2_LOCAL1, SCHEME_JDBC_H2_LOCAL2, SCHEME_FILE);

    private static final List<String> MEMORY = Arrays.asList(SCHEME_JDBC_H2_MEM);

    private static final List<String> REMOTE = Arrays.asList(SCHEME_JDBC_H2_TCP);

    /**
     * The url's path.
     */
    private String path;
    /**
     * the url's sheme.
     */
    private String scheme;
    /**
     * The file extension, either .h2.db or .mv.db
     */
    private String extension;

    /**
     * Create a new database url.
     * 
     * @param url
     */
    public H2DBDatabaseUrl(String url) throws MalformedURLException {
        if (url == null) {
            throw new NullPointerException();
        }

        // Try to find the scheme, path and file extension
        Pattern pattern = Pattern.compile("^(file:|jdbc:h2:file:|jdbc:h2:mem:|jdbc:h2:tcp:|jdbc:h2:)?(.+?)(\\.h2\\.db|\\.mv\\.db)?$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(url);
        if (!matcher.matches()) {
            throw new MalformedURLException("invalid URL: " + url); //$NON-NLS-1$
        }

        this.scheme = matcher.group(1);
        this.path = matcher.group(2);
        this.extension = matcher.group(3);
        if (isRemote() || isInMemory()) {
            // Leave nothing to do if memory or remote.
            this.extension = "";
            return;
        }

        // At this point we either have a local scheme or no scheme.
        this.scheme = SCHEME_JDBC_H2_LOCAL2;

        // Support url with MV_STORE=FALSE
        if (this.path.contains(";MV_STORE=FALSE")) {
            this.extension = ".h2.db";
        }

        // Remove the settings from the path.
        this.path = this.path.replaceFirst(";.*", "");

        // Determine the file extention. Checkif .h2.db exists, default to .mv.db
        if (this.extension == null) {
            this.extension = new File(this.path + ".h2.db").exists() ? ".h2.db" : ".mv.db";
        }

        // If the path is relative, we need to prefix with ./
        if (!new File(this.path).isAbsolute() && !this.path.startsWith("./")) {
            this.path = "./" + this.path;
        }

    }

    /**
     * Return reference to the local file if the URL is local or null if the url is not local.
     * 
     * @return
     */
    public File getAbsoluteFile() {
        if (!isLocal()) {
            throw new IllegalStateException("cannot return absolute path of remote database");
        }
        try {
            return new File(this.path + this.extension).getCanonicalFile();
        } catch (IOException e) {
            return new File(this.path + this.extension).getAbsoluteFile();
        }
    }

    /**
     * Return a string representation of a local database URL.
     * 
     * @return
     * @throws IllegalStateException
     *             if database is remote.
     */
    public String getAbsolutePath() {
        return getAbsoluteFile().getAbsolutePath();
    }

    /**
     * Return the database file name without suffix. (.h2.db)
     * 
     * @return
     */
    public String getName() {
        return new File(this.path).getName();
    }

    /**
     * Return the database path without the file suffix (.h2.db)
     * 
     * @return
     */
    public String getPath() {
        return this.path;
    }

    public boolean isInMemory() {
        return MEMORY.contains(this.scheme);
    }

    /**
     * Return true if the url is matching a local url pattern.
     * 
     * @return
     */
    public boolean isLocal() {
        return LOCAL.contains(this.scheme);
    }

    /**
     * Return true if the url define a in-memory database.
     * 
     * @return
     */
    @Deprecated
    public boolean isMemory() {
        return isInMemory();
    }

    /**
     * Return true if the url matches a remote database pattern.
     * 
     * @param url
     * @return
     */
    public boolean isRemote() {
        return REMOTE.contains(this.scheme);
    }

    /**
     * Return True if database should ise PageStore instead of MVStore.
     * 
     * @return
     * @throws if
     *             database is not local
     */
    public boolean isPageStore() {
        if (!isLocal()) {
            throw new IllegalStateException("cannot determine store of remote database");
        }
        return ".h2.db".equals(this.extension);
    }

    /**
     * Return a string representation of the url.
     */
    @Override
    public String toString() {
        return this.scheme + this.path;
    }

}