/**
 * Copyright(C) 2013 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.patrikdufresne.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TypedListener;

/**
 * Replacement for a TabFolder or a CTabFolder. This Button Tab Folder is using button as Tab.
 * 
 * <p>
 * The item children that may be added to instances of this class must be of type <code>BTabItem</code>.
 * <code>Control</code> children are created and then set into a tab item using <code>BTabItem#setControl</code>.
 * </p>
 * <p>
 * 
 * <p>
 * <dl>
 * <dt><b>Styles:</b></dt>
 * <dd>TOP, BOTTOM</dd>
 * <dt><b>Events:</b></dt>
 * <dd>Selection</dd>
 * </dl>
 * <p>
 * 
 * @author Patrik Dufresne
 *
 */
public class BTabFolder extends Composite {

    Composite bar;
    List<BTabItem> items = new ArrayList<>();
    Listener listener = new Listener() {
        @Override
        public void handleEvent(Event event) {
            if (event.widget.getData() instanceof BTabItem) {
                setSelection((BTabItem) event.widget.getData());
            }
        }
    };
    BTabItem selection;

    /**
     * 
     * @param parent
     * @param style
     */
    public BTabFolder(Composite parent, int style) {
        super(parent, checkStyle(style));
        // Create a custom layout
        super.setLayout(new BTabFolderLayout());
        // Create a composite to hold all the buttons.
        bar = new Composite(this, SWT.NONE);
        bar.setLayout(new RowLayout());
    }

    static int checkStyle(int style) {
        return style & (SWT.TOP);
    }

    /**
     * Adds the listener to the collection of listeners who will be notified when the user changes the receiver's
     * selection, by sending it one of the messages defined in the <code>SelectionListener</code> interface.
     * <p>
     * When <code>widgetSelected</code> is called, the item field of the event object is valid.
     * <code>widgetDefaultSelected</code> is not called.
     * </p>
     *
     * @param listener
     *            the listener which should be notified when the user changes the receiver's selection
     *
     * @exception IllegalArgumentException
     *                <ul>
     *                <li>ERROR_NULL_ARGUMENT - if the listener is null</li>
     *                </ul>
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     *
     * @see SelectionListener
     * @see #removeSelectionListener
     * @see SelectionEvent
     */
    public void addSelectionListener(SelectionListener listener) {
        checkWidget();
        if (listener == null) SWT.error(SWT.ERROR_NULL_ARGUMENT);
        TypedListener typedListener = new TypedListener(listener);
        addListener(SWT.Selection, typedListener);
        addListener(SWT.DefaultSelection, typedListener);
    }

    void createItem(BTabItem item, int index) {
        if (!(0 <= index && index <= items.size())) SWT.error(SWT.ERROR_INVALID_RANGE);
        // Create Button
        // TODO Move button to match index
        Button button = new Button(this.bar, SWT.TOGGLE);
        button.addListener(SWT.Selection, listener);
        button.setData(item);
        item.button = button;
        items.add(index, item);
        // Define this tab as selection is no select defined.
        if (selection == null) {
            setSelection(item);
        }
    }

    void destroyItem(BTabItem item) {
        if (!items.remove(item)) SWT.error(SWT.ERROR_ITEM_NOT_REMOVED);
        item.button.dispose();
        if (item == selection) {
            // Hide the control
            item.control.setVisible(false);
            selection = null;
            // Select a new item
            if (getItemCount() >= 0) {
                setSelection(0);
            }
        }
    }

    /**
     * Returns the item at the given, zero-relative index in the receiver. Throws an exception if the index is out of
     * range.
     *
     * @param index
     *            the index of the item to return
     * @return the item at the given index
     *
     * @exception IllegalArgumentException
     *                <ul>
     *                <li>ERROR_INVALID_RANGE - if the index is not between 0 and the number of elements in the list
     *                minus 1 (inclusive)</li>
     *                </ul>
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    public BTabItem getItem(int index) {
        checkWidget();
        return items.get(index);
    }

    /**
     * Returns the number of items contained in the receiver.
     *
     * @return the number of items
     *
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    public int getItemCount() {
        checkWidget();
        return items.size();
    }

    public BTabItem[] getItems() {
        checkWidget();
        return items.toArray(new BTabItem[items.size()]);
    }

    /**
     * Returns an array of <code>BTabItem</code>s that are currently selected in the receiver. An empty array indicates
     * that no items are selected.
     * <p>
     * Note: This is not the actual structure used by the receiver to maintain its selection, so modifying the array
     * will not affect the receiver.
     * </p>
     * 
     * @return an array representing the selection
     *
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    public BTabItem[] getSelection() {
        checkWidget();
        return new BTabItem[] { selection };
    }

    /**
     * Returns the zero-relative index of the item which is currently selected in the receiver, or -1 if no item is
     * selected.
     *
     * @return the index of the selected item
     *
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    public int getSelectionIndex() {
        checkWidget();
        if (selection == null) {
            return -1;
        }
        return indexOf(selection);
    }

    /**
     * Searches the receiver's list starting at the first item (index 0) until an item is found that is equal to the
     * argument, and returns the index of that item. If no item is found, returns -1.
     *
     * @param item
     *            the search item
     * @return the index of the item
     *
     * @exception IllegalArgumentException
     *                <ul>
     *                <li>ERROR_NULL_ARGUMENT - if the item is null</li>
     *                </ul>
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    public int indexOf(BTabItem item) {
        checkWidget();
        if (item == null) {
            SWT.error(SWT.ERROR_NULL_ARGUMENT);
        }
        return items.indexOf(item);
    }

    /**
     * Removes the listener from the collection of listeners who will be notified when the user changes the receiver's
     * selection.
     *
     * @param listener
     *            the listener which should no longer be notified
     *
     * @exception IllegalArgumentException
     *                <ul>
     *                <li>ERROR_NULL_ARGUMENT - if the listener is null</li>
     *                </ul>
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     *
     * @see SelectionListener
     * @see #addSelectionListener
     */
    public void removeSelectionListener(SelectionListener listener) {
        checkWidget();
        if (listener == null) SWT.error(SWT.ERROR_NULL_ARGUMENT);
        removeListener(SWT.Selection, listener);
        removeListener(SWT.DefaultSelection, listener);
    }

    /**
     * Sets the receiver's selection to the given item. The current selected is first cleared, then the new item is
     * selected.
     *
     * @param item
     *            the item to select
     *
     * @exception IllegalArgumentException
     *                <ul>
     *                <li>ERROR_NULL_ARGUMENT - if the item is null</li>
     *                </ul>
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     *
     * @since 3.2
     */
    public void setSelection(BTabItem item) {
        setSelection(item, true);
    }

    void setSelection(BTabItem item, boolean notify) {
        checkWidget();
        if (item == null) return;
        if (item == selection) return;
        if (items.indexOf(item) < 0) return;

        // Hide previous selection
        if (selection != null) {
            selection.button.setSelection(false);
            selection.control.setVisible(false);
        }
        item.button.setSelection(true);
        selection = item;
        if (item.control != null) {
            item.control.setVisible(true);
        }
        this.layout(true, true);
        if (notify) {
            Event event = new Event();
            event.item = item;
            notifyListeners(SWT.Selection, event);
        }
    }

    /**
     * Sets the receiver's selection to be the given array of items. The current selected is first cleared, then the new
     * items are selected.
     *
     * @param items
     *            the array of items
     *
     * @exception IllegalArgumentException
     *                <ul>
     *                <li>ERROR_NULL_ARGUMENT - if the items array is null</li>
     *                </ul>
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    public void setSelection(BTabItem[] items) {
        checkWidget();
        if (items == null) SWT.error(SWT.ERROR_NULL_ARGUMENT);
        if (items.length == 0) {
            setSelection(null, false);
        } else {
            for (int i = items.length - 1; i >= 0; --i) {
                setSelection(items[i], false);
            }
        }
    }

    /**
     * Selects the item at the given zero-relative index in the receiver. If the item at the index was already selected,
     * it remains selected. The current selection is first cleared, then the new items are selected. Indices that are
     * out of range are ignored.
     *
     * @param index
     *            the index of the item to select
     *
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    public void setSelection(int index) {
        checkWidget();
        if (!(0 <= index && index < getItemCount())) return;
        setSelection(getItem(index));
    }

    /**
     * Sets the layout which is associated with the receiver to be the argument which may be null.
     *
     * @param layout
     *            the receiver's new layout or null
     *
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    @Override
    public void setLayout(Layout layout) {
        checkWidget();
        // Do nothing. We need to keep the layout provided in the constructore of this class.
    }

}
