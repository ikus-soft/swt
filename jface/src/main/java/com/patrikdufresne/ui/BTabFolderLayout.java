/**
 * Copyright(C) 2013 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.patrikdufresne.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Layout;

class BTabFolderLayout extends Layout {

    /**
     * marginWidth specifies the number of points of horizontal margin that will be placed along the left and right
     * edges of the layout.
     *
     * The default value is 0.
     */
    public int marginWidth = 0;
    /**
     * marginHeight specifies the number of points of vertical margin that will be placed along the top and bottom edges
     * of the layout.
     *
     * The default value is 0.
     */
    public int marginHeight = 0;

    @Override
    protected Point computeSize(Composite composite, int wHint, int hHint, boolean flushCache) {
        Control children[] = composite.getChildren();
        // Content
        int maxWidth = 0;
        int maxHeight = 0;
        int barHeight = 0;
        for (int i = 0; i < children.length; i++) {
            Point size = children[i].computeSize(wHint, hHint, flushCache);
            maxWidth = Math.max(size.x, maxWidth);
            if (i == 0) {
                barHeight = size.y;
            } else {
                maxHeight = Math.max(size.y, maxHeight);
            }
        }
        int width = maxWidth + 2 * marginWidth;
        int height = barHeight + maxHeight + 2 * marginHeight;
        if (wHint != SWT.DEFAULT) width = wHint;
        if (hHint != SWT.DEFAULT) height = hHint;
        return new Point(width, height);
    }

    @Override
    protected boolean flushCache(Control control) {
        return true;
    }

    @Override
    protected void layout(Composite composite, boolean flushCache) {
        Control children[] = composite.getChildren();
        Rectangle rect = composite.getClientArea();
        rect.x += marginWidth;
        rect.y += marginHeight;
        rect.width -= 2 * marginWidth;
        rect.height -= 2 * marginHeight;
        for (int i = 0; i < children.length; i++) {
            if (i == 0) {
                Point size = children[i].computeSize(SWT.DEFAULT, SWT.DEFAULT, flushCache);
                children[i].setBounds(new Rectangle(rect.x + ((rect.width - size.x) / 2), rect.y, size.x, size.y));
                rect.y += size.y;
                rect.height -= size.y;
            } else {
                children[i].setBounds(rect);
            }
        }
    }

    String getName() {
        String string = getClass().getName();
        int index = string.lastIndexOf('.');
        if (index == -1) return string;
        return string.substring(index + 1, string.length());
    }

    /**
     * Returns a string containing a concise, human-readable description of the receiver.
     *
     * @return a string representation of the layout
     */
    @Override
    public String toString() {
        String string = getName() + " {";
        if (marginWidth != 0) string += "marginWidth=" + marginWidth + " ";
        if (marginHeight != 0) string += "marginHeight=" + marginHeight + " ";
        string = string.trim();
        string += "}";
        return string;
    }

}
