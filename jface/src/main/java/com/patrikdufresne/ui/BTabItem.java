/**
 * Copyright(C) 2013 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.patrikdufresne.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Widget;

/**
 * Instances of this class represent a selectable user interface object corresponding to a tab for a page in a tab
 * folder.
 * <dl>
 * <dt><b>Styles:</b></dt>
 * <dd>(none)</dd>
 * <dt><b>Events:</b></dt>
 * <dd>(none)</dd>
 * </dl>
 *
 */
public class BTabItem extends Item {

    Button button;
    Control control;
    private BTabFolder parent;

    /**
     * Constructs a new instance of this class given its parent (which must be a <code>BTabFolder</code>) and a style
     * value describing its behavior and appearance. The item is added to the end of the items maintained by its parent.
     * <p>
     * The style value is either one of the style constants defined in class <code>SWT</code> which is applicable to
     * instances of this class, or must be built by <em>bitwise OR</em>'ing together (that is, using the
     * <code>int</code> "|" operator) two or more of those <code>SWT</code> style constants. The class description lists
     * the style constants that are applicable to the class. Style bits are also inherited from superclasses.
     * </p>
     *
     * @param parent
     *            a composite control which will be the parent of the new instance (cannot be null)
     * @param style
     *            the style of control to construct
     *
     * @exception IllegalArgumentException
     *                <ul>
     *                <li>ERROR_NULL_ARGUMENT - if the parent is null</li>
     *                </ul>
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the parent</li>
     *                <li>ERROR_INVALID_SUBCLASS - if this class is not an allowed subclass</li>
     *                </ul>
     *
     * @see SWT
     * @see Widget#checkSubclass
     * @see Widget#getStyle
     */
    public BTabItem(BTabFolder parent, int style) {
        super(parent, style);
        this.parent = parent;
        parent.createItem(this, parent.getItemCount());
    }

    /**
     * Constructs a new instance of this class given its parent (which must be a <code>BTabFolder</code>), a style value
     * describing its behavior and appearance, and the index at which to place it in the items maintained by its parent.
     * <p>
     * The style value is either one of the style constants defined in class <code>SWT</code> which is applicable to
     * instances of this class, or must be built by <em>bitwise OR</em>'ing together (that is, using the
     * <code>int</code> "|" operator) two or more of those <code>SWT</code> style constants. The class description lists
     * the style constants that are applicable to the class. Style bits are also inherited from superclasses.
     * </p>
     *
     * @param parent
     *            a composite control which will be the parent of the new instance (cannot be null)
     * @param style
     *            the style of control to construct
     * @param index
     *            the zero-relative index to store the receiver in its parent
     *
     * @exception IllegalArgumentException
     *                <ul>
     *                <li>ERROR_NULL_ARGUMENT - if the parent is null</li>
     *                <li>ERROR_INVALID_RANGE - if the index is not between 0 and the number of elements in the parent
     *                (inclusive)</li>
     *                </ul>
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the parent</li>
     *                <li>ERROR_INVALID_SUBCLASS - if this class is not an allowed subclass</li>
     *                </ul>
     *
     * @see SWT
     * @see Widget#checkSubclass
     * @see Widget#getStyle
     */
    public BTabItem(BTabFolder parent, int style, int index) {
        super(parent, style, index);
        this.parent = parent;
        parent.createItem(this, index);
    }

    @Override
    public void dispose() {
        if (isDisposed()) return;
        // if (!isValidThread ()) error (SWT.ERROR_THREAD_INVALID_ACCESS);
        parent.destroyItem(this);
        super.dispose();
        parent = null;
        control = null;
        button = null;
    }

    /**
     * Returns the control that is used to fill the client area of the tab folder when the user selects the tab item. If
     * no control has been set, return <code>null</code>.
     * <p>
     * 
     * @return the control
     *
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    public Control getControl() {
        return control;
    }

    /**
     * Returns the receiver's parent, which must be a <code>TabFolder</code>.
     *
     * @return the receiver's parent
     *
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    public BTabFolder getParent() {
        checkWidget();
        return parent;
    }

    @Override
    public Image getImage() {
        return button.getImage();
    }

    @Override
    public String getText() {
        return button.getText();
    }

    /**
     * Returns the receiver's tool tip text, or null if it has not been set.
     *
     * @return the receiver's tool tip text
     *
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    public String getToolTipText() {
        return button.getToolTipText();
    }

    /**
     * Sets the control that is used to fill the client area of the tab folder when the user selects the tab item.
     * <p>
     * 
     * @param control
     *            the new control (or null)
     *
     * @exception IllegalArgumentException
     *                <ul>
     *                <li>ERROR_INVALID_ARGUMENT - if the control has been disposed</li>
     *                <li>ERROR_INVALID_PARENT - if the control is not in the same widget tree</li>
     *                </ul>
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    public void setControl(Control control) {
        checkWidget();
        if (control != null) {
            if (control.isDisposed()) SWT.error(SWT.ERROR_INVALID_ARGUMENT);
            if (control.getParent() != parent) SWT.error(SWT.ERROR_INVALID_PARENT);
        }

        this.control = control;

        // Sets the visibility and location of the component
        int index = parent.indexOf(this), selectionIndex = parent.getSelectionIndex();
        if (index != selectionIndex) {
            this.control.setVisible(false);
        } else {
            this.control.setVisible(true);
        }
        parent.layout(true, true);
    }

    @Override
    public void setImage(Image image) {
        button.setImage(image);
    }

    /**
     * Sets the receiver's text. The string may include the mnemonic character.
     * </p>
     * <p>
     * Mnemonics are indicated by an '&amp;' that causes the next character to be the mnemonic. When the user presses a
     * key sequence that matches the mnemonic, a selection event occurs. On most platforms, the mnemonic appears
     * underlined but may be emphasised in a platform specific manner. The mnemonic indicator character '&amp;' can be
     * escaped by doubling it in the string, causing a single '&amp;' to be displayed.
     * </p>
     *
     * @param string
     *            the new text
     *
     * @exception IllegalArgumentException
     *                <ul>
     *                <li>ERROR_NULL_ARGUMENT - if the text is null</li>
     *                </ul>
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     *
     */
    @Override
    public void setText(String text) {
        button.setText(text);
    }

    /**
     * Sets the receiver's tool tip text to the argument, which may be null indicating that the default tool tip for the
     * control will be shown. For a control that has a default tool tip, such as the Tree control on Windows, setting
     * the tool tip text to an empty string replaces the default, causing no tool tip text to be shown.
     * <p>
     * The mnemonic indicator (character '&amp;') is not displayed in a tool tip. To display a single '&amp;' in the
     * tool tip, the character '&amp;' can be escaped by doubling it in the string.
     * </p>
     * <p>
     * NOTE: This operation is a hint and behavior is platform specific, on Windows for CJK-style mnemonics of the form
     * " (&C)" at the end of the tooltip text are not shown in tooltip.
     * </p>
     *
     * @param string
     *            the new tool tip text (or null)
     *
     * @exception SWTException
     *                <ul>
     *                <li>ERROR_WIDGET_DISPOSED - if the receiver has been disposed</li>
     *                <li>ERROR_THREAD_INVALID_ACCESS - if not called from the thread that created the receiver</li>
     *                </ul>
     */
    public void setToolTipText(String titleToolTip) {
        button.setToolTipText(titleToolTip);
    }

}
