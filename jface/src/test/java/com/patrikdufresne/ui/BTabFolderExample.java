/**
 * Copyright(C) 2013 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.patrikdufresne.ui;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class BTabFolderExample extends Dialog {

    private BTabFolder tabFolder;

    protected BTabFolderExample(Shell parentShell) {
        super(parentShell);
    }

    @Override
    protected Point getInitialSize() {
        return new Point(450, 450);
    }

    int count = 2;

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite composite = (Composite) super.createDialogArea(parent);

        Button add = new Button(composite, SWT.NONE);
        add.setText("Add Tab");
        add.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                count++;
                BTabItem item = new BTabItem(tabFolder, SWT.NONE);
                item.setText("Tab " + count);
                item.setToolTipText("Tooltip Tab " + count);
                Text t = new Text(tabFolder, SWT.NONE);
                t.setText("TabItem Content: " + count);
                item.setControl(t);
            }

        });

        Button remove = new Button(composite, SWT.NONE);
        remove.setText("Remove Tab");
        remove.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                int idx = tabFolder.getSelectionIndex();
                if (idx >= 0) {
                    tabFolder.getItem(idx).dispose();
                }
            }

        });

        Label sep = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
        sep.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

        tabFolder = new BTabFolder(composite, SWT.BORDER);
        tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        BTabItem item1 = new BTabItem(tabFolder, SWT.NONE);
        item1.setText("Tab 1");
        item1.setToolTipText("Tooltip Tab 1");
        Text t = new Text(tabFolder, SWT.NONE);
        t.setText("TabItem Content: 1");
        item1.setControl(t);

        BTabItem item2 = new BTabItem(tabFolder, SWT.NONE);
        item2.setText("Tab 2");
        item2.setToolTipText("Tooltip Tab 2");
        t = new Text(tabFolder, SWT.NONE);
        t.setText("TabItem Content: 2");
        item2.setControl(t);

        // add controls to composite as necessary
        return composite;

    }

    public static void main(String[] args) {

        BTabFolderExample dlg = new BTabFolderExample(null);
        dlg.open();

    }

}
